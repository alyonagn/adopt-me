import { Link } from "react-router-dom";

export const ErrorFallbackComponent = () => {
  return (
    <h2>
      There was an error with this listing. <Link to="/">Click here</Link> to
      back to the home page.
    </h2>
  );
};
