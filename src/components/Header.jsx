import { Link } from "react-router-dom";

export const Header = () => {
  return (
    <Link to="/" className="header">
      Adopt Me!
    </Link>
  );
};
