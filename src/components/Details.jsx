import React, { useState, useContext } from "react";
import { useParams, useNavigate } from "react-router-dom";
import AdoptedPetContext from "../context/AdoptedPetContext";
import { useQuery } from "@tanstack/react-query";
import { fetchPet } from "../utils/fetchAPI";
import { Loader } from "./Loader";
import { Carousel } from "./Carousel";
import { ErrorBoundary } from "./ErrorBoundary";
import { Modal } from "./Modal";
import { Popup } from "./Popup";

export const Details = () => {
  const { id } = useParams();
  const results = useQuery(["details", id], fetchPet);
  const [showModal, setShowModal] = useState(false);
  const navigate = useNavigate();
  const [, setAdoptedPet] = useContext(AdoptedPetContext);

  if (results.isLoading) {
    return <Loader />;
  }

  const pet = results.data.pets[0];

  return (
    <div className="details">
      {showModal ? (
        <Modal>
          <Popup
            name={pet.name}
            onYes={() => {
              setAdoptedPet(pet);
              navigate("/");
            }}
            onNo={() => {
              setShowModal(false);
            }}
          />
        </Modal>
      ) : null}
      <Carousel images={pet.images} />
      <div className="details__text-container">
        <h1>{pet.name}</h1>
        <h2>{`${pet.animal} — ${pet.breed} — ${pet.city}, ${pet.state}`}</h2>
        <button onClick={() => setShowModal(true)}>Adopt {pet.name}</button>
        <p>{pet.description}</p>
      </div>
    </div>
  );
};

export function DetailsErrorBoundary(props) {
  return (
    <ErrorBoundary>
      <Details {...props} />
    </ErrorBoundary>
  );
}
