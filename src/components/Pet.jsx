import { Link } from "react-router-dom";

export const Pet = ({ name, animal, breed, images, location, id }) => {
  let hero = "http://pets-images.dev-apis.com/pets/none.jpg";
  if (images.length) {
    hero = images[0];
  }

  return (
    <Link to={`/details/${id}`} className="pet__container">
      <div className="pet__image-container">
        <img className="pet__image" src={hero} alt={name} />
      </div>
      <div className="pet__info">
        <h1 className="pet__name">{name}</h1>
        <h2 className="pet__location">{`${animal} — ${breed} — ${location}`}</h2>
      </div>
    </Link>
  );
};
