export const Loader = () => {
  return (
    <div className="loading-pane">
      <h2 className="loader">🐕</h2>
    </div>
  );
};
