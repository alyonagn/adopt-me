import { useContext, useState } from "react";
import AdoptedPetContext from "../context/AdoptedPetContext";
import { ANIMALS } from "../utils/data";
import { Pets } from "./Pets";
import { useQuery } from "@tanstack/react-query";
import { fetchSearch } from "../utils/fetchAPI";
import { useBreedList } from "../hooks/useBreedList";

export const Search = () => {
  const [requestParams, setRequestParams] = useState({
    location: "",
    animal: "",
    breed: "",
  });
  const [adoptedPet] = useContext(AdoptedPetContext);

  const [animal, setAnimal] = useState("");
  const [breeds] = useBreedList(animal);

  const results = useQuery(["search", requestParams], fetchSearch);
  const pets = results?.data?.pets ?? [];

  const handleSubmit = (e) => {
    e.preventDefault();
    const formData = new FormData(e.target);
    const obj = {
      animal: formData.get("animal") ?? "",
      breed: formData.get("breed") ?? "",
      location: formData.get("location") ?? "",
    };
    setRequestParams(obj);
  };

  return (
    <div className="search">
      <form className="search__form" onSubmit={handleSubmit}>
        {adoptedPet ? (
          <div className="pet__image-container">
            <img
              className="pet__image"
              src={adoptedPet.images[0]}
              alt={adoptedPet.name}
            />
          </div>
        ) : null}
        <label className="search__label" htmlFor="location">
          Location
          <input
            className="search__input"
            id="location"
            name="location"
            placeholder="Location"
          />
        </label>
        <label className="search__label" htmlFor="animal">
          Animal
          <select
            className="search__select"
            id="animal"
            name="animal"
            onChange={(e) => {
              setAnimal(e.target.value);
            }}
            onBlur={(e) => {
              setAnimal(e.target.value);
            }}
          >
            <option />
            {ANIMALS.map((animal) => (
              <option key={animal} value={animal}>
                {animal}
              </option>
            ))}
          </select>
        </label>
        <label className="search__label" htmlFor="breed">
          Breed
          <select
            className="search__select"
            disabled={!breeds.length}
            id="breed"
            name="breed"
          >
            <option />
            {breeds.map((breed) => (
              <option key={breed} value={breed}>
                {breed}
              </option>
            ))}
          </select>
        </label>
        <button className="search__button">Submit</button>
      </form>

      <Pets pets={pets} />
    </div>
  );
};
