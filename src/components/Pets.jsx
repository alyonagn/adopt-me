import React from "react";
import { Pet } from "./Pet";

export const Pets = ({ pets }) => {
  return (
    <div className="pets__container">
      {pets.length ? (
        pets.map((pet) => {
          const { name, id, breed, images, city, state } = pet;
          return (
            <Pet
              animal={pet.animal}
              key={id}
              name={name}
              breed={breed}
              images={images}
              location={`${city}, ${state}`}
              id={id}
            />
          );
        })
      ) : (
        <h1>Loading...</h1>
      )}
    </div>
  );
};
