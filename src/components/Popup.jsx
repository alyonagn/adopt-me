import React from "react";
export const Popup = ({ name, onYes, onNo }) => {
  return (
    <div className="popup__container">
      <h1>Would you like to adopt {name}?</h1>
      <div className="buttons">
        <button onClick={onYes} className="popup__button">
          Yes
        </button>
        <button onClick={onNo} className="popup__button">
          No
        </button>
      </div>
    </div>
  );
};
