import React, { useState } from "react";
import { createRoot } from "react-dom/client";
import { Search } from "./components/Search.jsx";
import { DetailsErrorBoundary } from "./components/Details.jsx";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { Header } from "./components/Header.jsx";
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import { ErrorFallbackComponent } from "./components/ErrorFallbackComponent.jsx";
import AdoptedPetContext from "./context/AdoptedPetContext";

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      staleTime: Infinity,
      cacheTime: Infinity,
    },
  },
});

const App = () => {
  const adoptedPet = useState(null);
  return (
    <div className="container">
      <QueryClientProvider client={queryClient}>
        <BrowserRouter>
          <AdoptedPetContext.Provider value={adoptedPet}>
            <Header />
            <Routes>
              <Route
                path="/details/:id"
                element={
                  <DetailsErrorBoundary
                    errorComponent={ErrorFallbackComponent}
                  />
                }
              />
              <Route path="/" element={<Search />} />
            </Routes>
          </AdoptedPetContext.Provider>
        </BrowserRouter>
      </QueryClientProvider>
    </div>
  );
};

const container = document.getElementById("root");
const root = createRoot(container);
root.render(React.createElement(App));
